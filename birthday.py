#!/usr/bin/python3
# Import library for getting time
import time
# Import libary for converting a date to epoch time
import datetime

year_of_birth = # Enter your year of birth here
month_of_birth = # Enter your month of birth here
day_of_birth = # Enter the day of your brith here

# Get time in seconds since the Unix epoch, Jan 1, 1970
today = time.time()
# Get birthday date in epoch time
birthday = datetime.datetime(year_of_birth, month_of_birth, day_of_birth, 0, 0).timestamp()
# Calculate my age in epoch time.
age = today - birthday

# Print my birthday message
print("To celebrate the fact that it was recently {today} seconds" \
      " since the epoch, and therefore {age} seconds since {birthday}" \
      " seconds since the epoch, there is cake in the kitchen. Enjoy!".format(
          today = int(today), age = int(age), birthday = int(birthday)))
