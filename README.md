# Birthday Calculator

Calculate your birthday in seconds since the Unix Epoch.

Try it online by copying and pasting the code to https://www.tutorialspoint.com/execute_python3_online.php

Here is a fun video on calculating time, with a bit of info about why the Unix Epoch.
https://www.youtube.com/watch?v=MVI87HzfskQ
